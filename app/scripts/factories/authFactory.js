'use strict';
angular.module('softvFrostApp')
	.factory('authFactory', function($http, $q, globalService, $base64, $localStorage, $location, $window, ngNotify) {
		var factory = {};
		var paths = {
			login: '/Usuario/LogOn',
			GetSP_DIMESITIENECONTRATACION :'/Usuario/GetSP_DIMESITIENECONTRATACION',
			datosLogueoDelContrato: '/freeAuth/GetDatosLogueo',
			GetLinkRegistro: '/freeAuth/GetLinkRegistro'
		};

		factory.GetSP_DIMESITIENECONTRATACION = function (contrato) {			
			var deferred = $q.defer();
			var Parametros = {
				'contrato': contrato			
			};						
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token
				}
			};		
			
			$http.post(globalService.getUrl() + paths.GetSP_DIMESITIENECONTRATACION, JSON.stringify(Parametros),config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});						

			return deferred.promise;
		};


		factory.login = function(user, password) {
			var token = $base64.encode(user + ':' + password);
			var deferred = $q.defer();
			var Parametros = {};
			var config = {
				headers: {
					'Authorization': 'Basic ' + token
				}
			};
			$http.post(globalService.getUrl() + paths.login, JSON.stringify(Parametros), config)
				.then(function(response) {
					if (response.data.LogOnResult.Token) {
						$localStorage.currentUser = {
							token: response.data.LogOnResult.Token,
							nombre: response.data.LogOnResult.Nombre,
							idRol: response.data.LogOnResult.IdRol,
							idUsuario: response.data.LogOnResult.IdUsuario,
							contrato:response.data.LogOnResult.Contrato,
							login: user,
							menu: response.data.LogOnResult.Menu,
							cliente: response.data.LogOnResult.Login
						};

						$localStorage.currentPay = {
							userLogueado: 0,
							userModal:0,
							idSession: null,
							logueoAutomatico: 0, //cuando se cerró la sesión pero ya hizo el pago
		                   // password: null,
		                   // userId: null,
		                   //merchantId: null,
		                    successIndicator:null,
		                    clvSessionCobra:null,
		                    resultIndicator:null,
		                    sessionVersion:null,
		                    urlCambiada:null
						};

						$localStorage.merchantData = {
							merchantId: null,
		                    userId: null,
		                    password: null,
		                    merchantName: null,
		                    addressLine1:null,
		                    addressLine2:null,
		                    email:null,
		                    descripcionImporte:null
						};
				
						deferred.resolve(true);

						
					} else {
						deferred.resolve(false);
					}
				})
				.catch(function(response) {
					console.log(response);
					if(response.status=== 302){
                        ngNotify.set('No puedes realizar un pago , debido a que tu servicio solo esta contratado, por favor contacta a atención a clientes.','warn');
					}else{
                        ngNotify.set('Autenticación inválida, credenciales no válidas.', 'error');
					}
					
					deferred.reject(response);
				});
			return deferred.promise;
		};




		factory.datosLogueoDelContrato = function (successIndicator, nivel, laUrl) {
			var deferred = $q.defer();
			var Parametros = {
				'id': 1,
				'successIndicator': successIndicator,
				'nivel': nivel,
				'laUrl': laUrl 
			};			
			var config = {
				headers: {
					'Authorization': '123','Unique':'custom'
				}
			};            
			$http.post(globalService.getUrl() + paths.datosLogueoDelContrato, JSON.stringify(Parametros),config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};


				


		factory.GetLinkRegistro = function () {		

			var deferred = $q.defer();
			var Parametros = {
				'id': 1			
			};				
			
			var config = {
				headers: {
					'Authorization': '123','Unique':'custom'
				}
			};

            
			$http.post(globalService.getUrl() + paths.GetLinkRegistro, JSON.stringify(Parametros),config).then(function (response) {
				deferred.resolve(response.data);
			}).catch(function (response) {
				deferred.reject(response);
			});
			

			return deferred.promise;
		};









		return factory;
	});
