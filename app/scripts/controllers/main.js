'use strict';

/**
 * @ngdoc function
 * @name softvFrostApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the softvFrostApp
 */
angular.module('softvFrostApp')
	.controller('MainCtrl', function($localStorage, $window, $location, rolFactory, $uibModal) {
		this.awesomeThings = [
			'HTML5 Boilerplate',
			'AngularJS',
			'Karma'
		];
		this.$onInit = function() {
			if ($localStorage.currentUser) {
				vm.menus = $localStorage.currentUser.menu;
				vm.usuario = $localStorage.currentUser.cliente;
				
				rolFactory.GetRoleList().then(function(data) {
					data.GetRoleListResult.forEach(function(item) {
						if (item.IdRol === $localStorage.currentUser.idRol) {
							vm.rol = item.Nombre;
						}
					});
				});
			} else {
				$location.path('/auth/login');
			}

		};

		function logOut() {
			delete $localStorage.currentUser;
			$window.location.reload();
		}

		
    	function cambiarEmail() {           
       	
            vm.animationsEnabled = true;
            var modalInstance = $uibModal.open({
                animation: vm.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/pagoEnLinea/modalAparato.html',
                controller: 'ModalCorreoCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg'                
            });
    	}




		var vm = this;
		vm.logOut = logOut;
		vm.cambiarEmail = cambiarEmail;
	});
