'use strict';
angular
	.module('softvFrostApp')
	.controller('ModalCorreoCtrl', 
		function($uibModal, $uibModalInstance, pagoEnLineaFactory,  $localStorage, ngNotify, $window) {
	
		function init() {		
			
		}	

        function logOut() {         
            delete $localStorage.currentUser;
            $window.location.reload();
        }

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		    //logOut();
		}

		function cambiarEmail() {		
			try{
				if( validarEmail(vm.correoActual) )
				{		
					pagoEnLineaFactory.GetValidaCorreo(vm.correoActual).then(function(data) {   
			 		  	
						if (data.GetValidaCorreoResult.EsValido == 1)
						{
							pagoEnLineaFactory.getUpdateCliente(contrato, vm.correoActual).then(function(data) {   
					 			if (data.GetUpdateClienteResult == 1)
					 			{           
						            ngNotify.set('Correo actualizado. Inicie sesión con este correo la siguiente vez.', 'success');
						       		$localStorage.currentUser.login = vm.correoActual;				       		
						        }
						        else{
						            ngNotify.set('Error, vuelva a intentarlo.', 'error');
						        }	            
					        });						        
						}
						else{
   							ngNotify.set('Este correo ya está ligado a un contrato.', 'error');	
						}					      
			        });					
				}
				else
				{
				    ngNotify.set('Ingrese un correo válido.', 'error');
				}				
			}
			catch(ex)
			{
				ngNotify.set('Ha terminado su sesión, acceda e intente de nuevo', 'error');
				setTimeout( function() {logOut()},3000);				
			}
		}
		

		function validarEmail(email) {			
			var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    		return regex.test(email) ? true : false;			
		}


		function consultarAyuda() {
			if (vm.mostrar == 0)
			{
				vm.mostrar = 1;
				vm.titulo = "Identifica la serie de tu aparato"
			}else 
			{
				vm.mostrar = 0;
				vm.titulo = "Cambiar correo electrónico";
			}			
      	}


      	function validarSerie(){
			try{
      			if (vm.numeroSerie){
      				pagoEnLineaFactory.dameCorreo(contrato).then(function(data) {				
	 					vm.correoActual = data.GetCorreoResult.Correo; 
	 					pagoEnLineaFactory.GetValidaRecoverPassword(vm.numeroSerie, vm.correoActual).then(function(data) {   
	   						   				
					 		if (data.GetValidaRecoverPasswordResult >= 1)
					 		{           
						         ngNotify.set('Serie validada', 'success');
						         vm.mostrar = 2;
						         vm.titulo = 'Proceso cambio de correo electrónico';
						        
						    }
						    else{
						    	ngNotify.set('La serie no tiene relación con el contrato actual.', 'error');
						    }	           
			   		    });	
	            	});	
      			}
      			else {
      				ngNotify.set('El campo serie es obligatorio.', 'error');	
      			}	
			}
			catch(ex)
			{
				ngNotify.set('Ha terminado su sesión, acceda e intente de nuevo', 'error');
				setTimeout( function() {logOut()},3000);				
			}


  
      	}


		var vm = this;
		vm.cancel = cancel;
		vm.cambiarEmail = cambiarEmail;
		vm.consultarAyuda = consultarAyuda;
		vm.validarEmail = validarEmail;
		vm.numeroSerie = vm.numeroSerie;
		vm.mostrar = 0; // 0:normal | 1:ayuda | 2:correo
		vm.validarSerie = validarSerie
		vm.titulo = "Cambiar correo electrónico";			
		var contrato = $localStorage.currentUser.contrato;	 


		var img = new Image();
	
		init();

	});


