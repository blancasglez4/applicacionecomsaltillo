'use strict';
angular.module('softvFrostApp')
    .controller('PagoReciboPublicoCtrl', function ($uibModal, $state, $rootScope, ngNotify, $localStorage, $window, pagoReciboFactory, globalService, $sce, $http, $stateParams) // servicio
    {
        var vm = this;
        var ContratoComp;
        var ContratoReal;
        var NombreCliente;

        vm.textoPantalla =' ';

        this.$onInit = function () {   
            // url de respuesta
            console.log('PAGO RECIBO PUBLICO');

            // TOMAMOS LA URL QUE REDIRECCIONA AL PAGO -- recibo PUBLICO 
            var url1 = " "; 
            url1 = window.location.href;

             // Revisamos las variables del navegador 
            var urlnavegador = window.location.href;
            var n = urlnavegador.indexOf("resultIndicator");
            var url = new URL(urlnavegador);  

            if (n == -1)  // No existe resultIndicator en URL    
            {             
                vm.textoPantalla = "¡Ups! Hubo problemas al guardar el pago en nuestro sistema, favor de comunicarse a las oficinas con su comprobante de pago bancario.";
                delete $localStorage.currentUser;
                setTimeout( function() { 
                    $state.go('login'); 
                }, 10000); 
            }
            else  // INICIA GUARDAR PAGO  
            {                           
                pagoReciboFactory.guardaRedireccionPublico(1, 0, 0, '---', urlnavegador).then(function (data) {
                    vm.textoPantalla = "Pago guardado. Por favor verifique en su correo electrónico.";
                    delete $localStorage.currentUser;
                    setTimeout( function() { 
                        $state.go('login'); 
                    }, 10000);  
                });
            }                 
        }      
    });
